<?php

namespace We7\V182;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1544682195
 * @version 1.8.2
 */

class UpdateUniSettings {

	/**
	 *  执行更新
	 */
	public function up() {
		$all_uni_settings = pdo_getall('uni_settings', array(), array('oauth', 'uniacid'));
		if (!empty($all_uni_settings)) {
			foreach($all_uni_settings as $setting) {
				$setting['oauth'] = iunserializer($setting['oauth']);
				if (empty($setting['oauth'])) {
					continue;
				}
				if (empty($setting['oauth']['host']) && empty($setting['oauth']['account'])) {
					pdo_update('uni_settings', array('oauth' => ''), array('uniacid' => $setting['uniacid']));
				}
			}

		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		