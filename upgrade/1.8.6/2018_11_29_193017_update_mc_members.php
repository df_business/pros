<?php

namespace We7\V186;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1543491017
 * @version 1.8.6
 */

class UpdateMcMembers {

	/**
	 *  执行更新
	 */
	public function up() {
		if(!pdo_fieldexists('mc_members', 'createtime')) {
			pdo_query("ALTER TABLE " . tablename('mc_members') . " ADD INDEX(`createtime`);");
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		