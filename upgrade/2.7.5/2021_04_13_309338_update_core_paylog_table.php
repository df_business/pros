<?php

namespace We7\V275;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1618309338
 * @version 2.7.5
 */

class UpdateCorePaylogTable {

	/**
	 *  执行更新
	 */
	public function up() {
		if (!pdo_fieldexists('core_paylog', 'coupon')) {
			pdo_query("ALTER TABLE " . tablename('core_paylog') . " ADD `coupon` VARCHAR(1000) NOT NULL;");
		}
	}

	/**
	 *  回滚更新
	 */
	public function down() {


	}
}
