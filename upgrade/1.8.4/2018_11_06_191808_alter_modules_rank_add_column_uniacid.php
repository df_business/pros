<?php

namespace We7\V184;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1541503088
 * @version 1.8.4
 */

class AlterModulesRankAddColumnUniacid {

	/**
	 *  执行更新
	 */
	public function up() {
		if (!pdo_fieldexists('modules_rank', 'uniacid')) {
			pdo_query("ALTER TABLE " . tablename('modules_rank') . " ADD `uniacid` int(10) NOT NULL DEFAULT 0 ;");
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {

	}
}
		