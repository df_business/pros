<?php

namespace We7\V184;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1541907557
 * @version 1.8.4
 */

class CreateTableUniAccountModulesShortcut {

	/**
	 *  执行更新
	 */
	public function up() {
		if (!pdo_tableexists('uni_account_modules_shortcut')) {
			$table_name = tablename('uni_account_modules_shortcut');
			$sql = <<<EOF
CREATE TABLE $table_name (
	`id` int(10) unsigned not null AUTO_INCREMENT,
	`title` varchar(200) NOT NULL DEFAULT '' COMMENT '快捷入口名称',
	`url` varchar(250) NOT NULL DEFAULT '' COMMENT '快捷入口链接',
	`icon` varchar(200) NOT NULL DEFAULT '' COMMENT '快捷入口图标',
	`uniacid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '帐号uniacid',
	`version_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '帐号version_id',
	`module_name` varchar(200) NOT NULL DEFAULT '' COMMENT '模块名称',
	PRIMARY KEY(`id`)
) DEFAULT CHARSET=utf8;
EOF;
			pdo_query($sql);
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		