<?php

namespace We7\V202;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1552556483
 * @version 2.0.2
 */

class UpgradeUniSettings {

	/**
	 *  执行更新
	 */
	public function up() {
		if (pdo_fieldexists('uni_settings','uc')) {
			pdo_query("ALTER TABLE " . tablename('uni_settings') . " CHANGE `uc` `uc` VARCHAR(700) NOT NULL");
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		