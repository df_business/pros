<?php

namespace We7\V182;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1536148805
 * @version 1.8.2
 */

class AlterMassRecordAddColumns {

	/**
	 *  执行更新
	 */
	public function up() {
		if (!pdo_fieldexists('mc_mass_record', array('msg_id', 'msg_data_id'))) {
			$table_name = tablename('mc_mass_record');
			$sql = <<<EOT
				ALTER TABLE {$table_name} ADD `msg_id` varchar(50) NOT NULL DEFAULT '' COMMENT '消息发送任务的ID';
				ALTER TABLE {$table_name} ADD `msg_data_id` varchar(50) NOT NULL DEFAULT '' COMMENT '消息的数据ID，，该字段只有在群发图文消息时，才会出现。可以用于在图文分析数据接口中，获取到对应的图文消息的数据，是图文分析数据接口中的msgid字段中的前半部分，详见图文分析数据接口中的msgid字段的介绍';
EOT;
			pdo_run($sql);
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		