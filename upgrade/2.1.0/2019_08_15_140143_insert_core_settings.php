<?php

namespace We7\V210;

defined('IN_IA') or exit('Access Denied');
/**
* [WeEngine System] Copyright (c) 2014 W7.CC
* Time: 1565848903
* @version 2.1.0
*/

class InsertCoreSettings {

	/**
	 *  执行更新
	 */
	public function up() {
		$is_exist = table('core_settings')->where(array('key' => 'system_module_expire'))->get();
		if (empty($is_exist)) {
			pdo_insert('core_settings', array('key'=> 'system_module_expire', 'value' => '您访问的功能模块不存在，请重新进入'), TRUE);
		}
	}

	/**
	 *  回滚更新
	 */
	public function down() {


	}
}
