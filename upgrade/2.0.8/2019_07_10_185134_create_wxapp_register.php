<?php

namespace We7\V208;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1562755894
 * @version 2.0.8
 */

class CreateWxappRegister {

	/**
	 *  执行更新
	 */
	public function up() {
		if (!pdo_tableexists('wxapp_register')) {
			$table_name = tablename('wxapp_register');
			$sql = <<<EOF
CREATE TABLE IF NOT EXISTS $table_name (
	`id` int(10) unsigned not null AUTO_INCREMENT,
	`component_appid` varchar(100) not null DEFAULT '' COMMENT '第三方平台appid',
	`name` varchar(100) not null DEFAULT '' COMMENT '企业名',
	`code` varchar(50) not null DEFAULT '' COMMENT '企业代码',
	`code_type` int(11) not null DEFAULT 0 COMMENT '企业代码类型:1,统一社会信用代码（18位）2,组织机构代码（9位xxxxxxxx-x）3,营业执照注册号(15位)',
	`legal_persona_wechat` varchar(100) not null DEFAULT '' COMMENT '法人微信号',
	`legal_persona_name` varchar(100) not null DEFAULT '' COMMENT '法人姓名',
	`component_phone` varchar(100) not null DEFAULT '' COMMENT '第三方联系电话',
	`status` tinyint(4) not null DEFAULT '0' COMMENT '状态；1待审核，2审核通过，3审核未通过',
	`message` varchar(200) not null DEFAULT '' COMMENT '审核未通过原因',
	`auth_code` VARCHAR(255) not null DEFAULT '' COMMENT '第三方授权码',
	`appid` VARCHAR(255) not null DEFAULT '' COMMENT '创建的小程序appid',
	`acid` int(11) not null DEFAULT '0' COMMENT '关联平台的acid',
	`uniacid` int(11) not null DEFAULT '0' COMMENT '关联平台的uniacid',
	PRIMARY KEY(`id`),
	KEY `uniacid` (`uniacid`)
) DEFAULT CHARSET=utf8 COMMENT='注册小程序记录表';
EOF;
			pdo_query($sql);
		}
		if (!pdo_tableexists('wxapp_register_version')) {
			$table_name = tablename('wxapp_register_version');
			$sql = <<<EOF
CREATE TABLE IF NOT EXISTS $table_name (
	`id` int(11) unsigned not null AUTO_INCREMENT,
	`uniacid` int(11) not null COMMENT '所属账号uniacid',
	`auditid` int(11) not null COMMENT '审核ID',
	`version` varchar(20) not null DEFAULT '' COMMENT '版本号',
	`description` varchar(255) not null DEFAULT '' COMMENT '版本描述',
	`status` tinyint(1) not null DEFAULT 0 COMMENT '0体验版；5为审核通过，待发布;1为审核失败;2为审核中;3已撤回；4发布成功',
	`reason` varchar(1000) not null DEFAULT '' COMMENT '审核失败原因',
	`upload_time` int(11) not null COMMENT '提交审核时间',
	`audit_info` text COMMENT '审核的版本信息',
	`submit_info` text COMMENT '上线的版本信息',
	`developer` varchar(50) not null DEFAULT '' COMMENT '提交代码的开发者微信名',
	PRIMARY KEY(`id`),
	KEY `uniacid` (`uniacid`),
	KEY `auditid` (`auditid`)
) DEFAULT CHARSET=utf8 COMMENT='注册小程序版本信息表';
EOF;
			pdo_query($sql);
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		