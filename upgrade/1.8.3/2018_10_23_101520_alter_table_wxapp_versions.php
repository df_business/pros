<?php

namespace We7\V183;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1540260920
 * @version 1.8.3
 */

class AlterTableWxappVersions {

	/**
	 *  执行更新
	 */
	public function up() {
		if(!pdo_fieldexists('wxapp_versions', 'upload_time')) {
			pdo_query("ALTER TABLE " . tablename('wxapp_versions') . " ADD `upload_time` INT(10) NOT NULL DEFAULT 0 COMMENT '最后上传时间'");
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		