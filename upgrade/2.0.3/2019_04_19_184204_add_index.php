<?php

namespace We7\V203;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1555670524
 * @version 2.0.3
 */

class AddIndex {

	/**
	 *  执行更新
	 */
	public function up() {
		if(!pdo_indexexists('uni_account_users', 'uid')){
			pdo_query("ALTER TABLE " .tablename('uni_account_users') ." ADD INDEX uid (`uid`)");
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		