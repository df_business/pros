<?php

namespace We7\V210;

defined('IN_IA') or exit('Access Denied');
/**
* [WeEngine System] Copyright (c) 2014 W7.CC
* Time: 1565754242
* @version 2.1.0
*/

class DropMcMappingUcenter {

	/**
	 *  执行更新
	 */
	public function up() {
		if(pdo_tableexists('mc_mapping_ucenter')) {
			$sql = "DROP TABLE IF EXISTS " . tablename('mc_mapping_ucenter');
			pdo_run($sql);
		}
	}

	/**
	 *  回滚更新
	 */
	public function down() {


	}
}
