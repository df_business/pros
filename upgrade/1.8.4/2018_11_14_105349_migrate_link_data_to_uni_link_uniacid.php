<?php

namespace We7\V184;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1542164029
 * @version 1.8.4
 */

class MigrateLinkDataToUniLinkUniacid {

	/**
	 *  执行更新
	 */
	public function up() {
		$account_modules_data = pdo_getall('uni_account_modules', array('settings <>' => ''), array('id', 'uniacid', 'module', 'settings'));
		if (!empty($account_modules_data)) {
			foreach ($account_modules_data as $item) {
				$settings = iunserializer($item['settings']);

				if (!empty($settings['link_uniacid'])) {
					pdo_insert('uni_link_uniacid', array(
						'uniacid' => $item['uniacid'],
						'module_name' => $item['module'],
						'link_uniacid' => $settings['link_uniacid'],
					));
					unset($settings['link_uniacid']);
					$settings = empty($settings) ? '' : iserializer($settings);
					pdo_update('uni_account_modules', array('settings' => $settings), array('id' => $item['id']));
				}

				if (!empty($settings['passive_link_uniacid'])) {
					unset($settings['passive_link_uniacid']);
					$settings = empty($settings) ? '' : iserializer($settings);
					pdo_update('uni_account_modules', array('settings' => $settings), array('id' => $item['id']));
				}
			}
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		