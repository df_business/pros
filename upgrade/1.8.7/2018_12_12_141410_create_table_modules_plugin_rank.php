<?php

namespace We7\V187;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1544595250
 * @version 1.8.7
 */

class CreateTableModulesPluginRank {

	/**
	 *  执行更新
	 */
	public function up() {
		if (!pdo_tableexists('modules_plugin_rank')) {
			$table_name = tablename('modules_plugin_rank');
			$sql = <<<EOF
CREATE TABLE $table_name (
	`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`uniacid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '默认账号uniacid',
	`uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
	`rank` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
	`plugin_name` varchar(200) NOT NULL DEFAULT '' COMMENT '插件名称',
	`main_module_name` varchar(200) NOT NULL DEFAULT '' COMMENT '主应用名称',
	PRIMARY KEY(`id`)
) DEFAULT CHARSET=utf8;
EOF;
			pdo_query($sql);
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		