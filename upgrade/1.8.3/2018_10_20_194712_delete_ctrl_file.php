<?php

namespace We7\V183;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1540036032
 * @version 1.8.3
 */

class DeleteCtrlFile {

	/**
	 *  执行更新
	 */
	public function up() {
		$files = array(
			IA_ROOT . '/framework/model/webapp.mod.php',
			IA_ROOT . '/framework/table/articlecategory.table.php',
			IA_ROOT . '/framework/table/articlenews.table.php',
			IA_ROOT . '/framework/table/articlenotice.table.php',
			IA_ROOT . '/framework/table/phoneappversions.table.php',
			IA_ROOT . '/framework/table/sitearticlecomment.table.php',
			IA_ROOT . '/framework/table/sitetemplates.table.php',
			IA_ROOT . '/framework/table/userspermission.table.php',
			IA_ROOT . '/web/source/miniapp/display.ctrl.php',
			IA_ROOT . '/web/source/platform/stat.ctrl.php',
			IA_ROOT . '/web/source/wxapp/display.ctrl.php',
			IA_ROOT . '/web/source/wxapp/version.ctrl.php',
			IA_ROOT . '/web/source/webapp/module-link-uniacid.ctrl.php',
			IA_ROOT . '/web/themes/default/account/manage-modules-tpl-aliapp.html',
			IA_ROOT . '/web/themes/default/account/manage-modules-tpl-phoneapp.html',
			IA_ROOT . '/web/themes/default/account/manage-modules-tpl-webapp.html',
			IA_ROOT . '/web/themes/default/account/manage-modules-tpl-wxapp.html',
			IA_ROOT . '/web/themes/default/account/manage-modules-tpl-xzapp.html',
			IA_ROOT . '/web/themes/default/common/header-webapp.html',
			IA_ROOT . '/web/themes/default/common/header-xzapp.html',
			IA_ROOT . '/web/themes/default/webapp/module-link-uniacid.html',
			IA_ROOT . '/web/themes/default/wxapp/version-display.html',
			IA_ROOT . '/web/themes/default/wxapp/version-home.html',
		);
		foreach ($files as $file) {
			if (file_exists($file)) {
				unlink($file);
			}
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		