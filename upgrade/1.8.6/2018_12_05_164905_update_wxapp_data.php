<?php

namespace We7\V186;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1543999745
 * @version 1.8.6
 */

class UpdateWxappData {

	/**
	 *  执行更新
	 */
	public function up() {
		$all_wxapp_settings = pdo_fetchall("SELECT wx.uniacid, s.uniacid as id, s.creditnames, s.creditbehaviors FROM " . tablename('account_wxapp') . " AS wx LEFT JOIN " . tablename('uni_settings') . " AS s ON wx.uniacid = s.uniacid");

		if (!empty($all_wxapp_settings)) {
			foreach ($all_wxapp_settings as $wxapp) {
				$unisettings = array();
				if (empty($wxapp['creditnames'])) {
					$unisettings['creditnames'] = array('credit1' => array('title' => '积分', 'enabled' => 1), 'credit2' => array('title' => '余额', 'enabled' => 1));
					$unisettings['creditnames'] = iserializer($unisettings['creditnames']);
				}
				if (empty($wxapp['creditbehaviors'])) {
					$unisettings['creditbehaviors'] = array('activity' => 'credit1', 'currency' => 'credit2');
					$unisettings['creditbehaviors'] = iserializer($unisettings['creditbehaviors']);
				}
				if (empty($unisettings)) {
					continue;
				}
				if (empty($wxapp['id'])) {
					$unisettings['uniacid'] = $wxapp['uniacid'];
					pdo_insert('uni_settings', $unisettings);
				} else {
					pdo_update('uni_settings', $unisettings, array('uniacid' => $wxapp['uniacid']));
				}
			}
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		