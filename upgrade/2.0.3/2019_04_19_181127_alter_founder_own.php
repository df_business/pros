<?php

namespace We7\V203;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1555668687
 * @version 2.0.3
 */

class AlterFounderOwn {

	/**
	 *  执行更新
	 */
	public function up() {
		if(!pdo_indexexists('users_founder_own_users', 'uid')){
			pdo_query("ALTER TABLE " .tablename('users_founder_own_users') ." ADD INDEX uid (`uid`)");
		}
		if(!pdo_indexexists('users_founder_own_users', 'founder_uid')){
			pdo_query("ALTER TABLE " .tablename('users_founder_own_users') . " ADD INDEX founder_uid (`founder_uid`)");
		}

		if(!pdo_indexexists('users_founder_own_users_groups', 'founder_uid')){
			pdo_query("ALTER TABLE " .tablename('users_founder_own_users_groups') ." ADD INDEX founder_uid (`founder_uid`)");
		}
		if(!pdo_indexexists('users_founder_own_users_groups', 'users_group_id')){
			pdo_query("ALTER TABLE " .tablename('users_founder_own_users_groups') . " ADD INDEX users_group_id (`users_group_id`)");
		}

		if(!pdo_indexexists('users_founder_own_uni_groups', 'founder_uid')){
			pdo_query("ALTER TABLE " .tablename('users_founder_own_uni_groups') . " ADD INDEX founder_uid (`founder_uid`)");
		}
		if(!pdo_indexexists('users_founder_own_uni_groups', 'uni_group_id')){
			pdo_query("ALTER TABLE " .tablename('users_founder_own_uni_groups') . " ADD INDEX uni_group_id (`uni_group_id`)");
		}

		if(!pdo_indexexists('users_founder_own_create_groups', 'founder_uid')){
			pdo_query("ALTER TABLE " .tablename('users_founder_own_create_groups') . " ADD INDEX founder_uid (`founder_uid`)");
		}
		if(!pdo_indexexists('users_founder_own_create_groups', 'create_group_id')){
			pdo_query("ALTER TABLE " .tablename('users_founder_own_create_groups') . " ADD INDEX create_group_id (`create_group_id`)");
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		