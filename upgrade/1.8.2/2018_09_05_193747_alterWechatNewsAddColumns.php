<?php

namespace We7\V182;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1536147467
 * @version 1.8.2
 */

class AlterWechatNewsAddColumns {

	/**
	 *  执行更新
	 */
	public function up() {
		if (!pdo_fieldexists('wechat_news', array('need_open_comment', 'only_fans_can_comment'))) {
			$table_name = tablename('wechat_news');
			$sql = <<<EOT
				ALTER TABLE {$table_name} ADD `need_open_comment` tinyint(1) NOT NULL DEFAULT 1 COMMENT '是否打开评论，0不打开，1打开';
				ALTER TABLE {$table_name} ADD `only_fans_can_comment` tinyint(1) NOT NULL DEFAULT 1 COMMENT '是否粉丝才可评论，0所有人可评论，1粉丝才可评论';
EOT;
			pdo_run($sql);
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		