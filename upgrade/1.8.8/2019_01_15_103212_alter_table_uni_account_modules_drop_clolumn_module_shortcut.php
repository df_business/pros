<?php

namespace We7\V188;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1547519532
 * @version 1.8.8
 */

class AlterTableUniAccountModulesDropClolumnModuleShortcut {

	/**
	 *  执行更新
	 */
	public function up() {
		if (pdo_fieldexists('uni_account_modules', 'module_shortcut')) {
			$sql = "ALTER TABLE " . tablename('uni_account_modules') . " DROP COLUMN `module_shortcut`";
			pdo_run($sql);
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		