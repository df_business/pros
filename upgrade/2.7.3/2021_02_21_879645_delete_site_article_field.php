<?php

namespace We7\V273;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1613879645
 * @version 2.7.3
 */

class DeleteSiteArticleField {

	/**
	 *  执行更新
	 */
	public function up() {
		if (pdo_fieldexists('site_article', 'kid')) {
			pdo_query("ALTER TABLE " . tablename('site_article') . " DROP COLUMN `kid`;");
		}
	}

	/**
	 *  回滚更新
	 */
	public function down() {


	}
}
