<?php

namespace We7\V183;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1540193866
 * @version 1.8.3
 */

class CreateTableUsersExtraLimit {

	/**
	 *  执行更新
	 */
	public function up() {
		if (!pdo_tableexists('users_extra_limit')) {
			$table_name = tablename('users_extra_limit');
			$sql = <<<EOF
CREATE TABLE $table_name (
	`id` int(10) unsigned not null AUTO_INCREMENT,
	`uid` int(10) unsigned not null DEFAULT '0' COMMENT '用户id',
	`maxaccount` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '可创建的公众号数量',
	`maxwxapp` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '可创建的小程序数量',
	`maxwebapp` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '可创建的PC数量',
	`maxphoneapp` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '可创建的APP数量',
	`maxxzapp` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '可创建的熊掌号数量',
	`maxaliapp` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '可创建的支付宝小程序数量',
	`timelimit` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '有效时间',
	PRIMARY KEY(`id`)
) DEFAULT CHARSET=utf8;
EOF;
			pdo_query($sql);
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		