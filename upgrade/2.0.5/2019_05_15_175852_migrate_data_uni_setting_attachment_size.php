<?php

namespace We7\V205;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1557914332
 * @version 2.0.5
 */

class MigrateDataUniSettingAttachmentSize {

	/**
	 *  执行更新
	 */
	public function up() {
		global $_W;
		if (pdo_fieldexists('uni_settings', 'attachment_size')) {
			pdo_update('uni_settings', array('attachment_size' => 0));
			$attachdir = glob(IA_ROOT . '/' . $_W['config']['upload']['attachdir'] . '/*');
			if (!empty($attachdir)) {
				foreach ($attachdir as $attach) {
					if (!is_dir($attach)) {
						continue;
					}
					$attach = glob($attach . '/*');
					foreach ($attach as $dir) {
						if (!is_dir($dir)) {
							continue;
						}
						$uniacid = substr($dir, strripos($dir, '/') + 1);
						$uniacid = pdo_getcolumn('account', array('uniacid' => $uniacid), 'uniacid');
						if (!empty($uniacid)) {
							$size = dir_size($dir);
							$size = round($size / 1024);
							$setting_info = pdo_get('uni_settings', array('uniacid' => $uniacid), array('uniacid', 'attachment_size'));
							$attachment_size = empty($setting_info['attachment_size']) ? 0 : $setting_info['attachment_size'];
							if (empty($setting_info)) {
								pdo_insert('uni_settings', array('attachment_size' => $size, 'uniacid' => $uniacid));
							} else {
								pdo_update('uni_settings', array('attachment_size' => $size + $attachment_size), array('uniacid' => $uniacid));
							}
						}
					}
				}
			}
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		