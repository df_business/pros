<?php

namespace We7\V210;

defined('IN_IA') or exit('Access Denied');
/**
* [WeEngine System] Copyright (c) 2014 W7.CC
* Time: 1571741779
* @version 2.1.0
*/

class UpdateAccountToutiaoapp {

	/**
	 *  执行更新
	 */
	public function up() {
		if (pdo_fieldexists('account_toutiaoapp','key')) {
			pdo_query("ALTER TABLE " . tablename('account_toutiaoapp') . " MODIFY COLUMN `secret` varchar(40) NOT NULL ;");
		}
	}

	/**
	 *  回滚更新
	 */
	public function down() {


	}
}
