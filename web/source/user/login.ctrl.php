<?php
/**
 * 用户登录
 * [WeEngine System] Copyright (c) 2014 W7.CC.
 */
defined('IN_IA') or exit('Access Denied');
define('IN_GW', true);

load()->model('message');
load()->model('utility');
if (!empty($_GPC['code'])) {
	//强制指定为true，用于message跳转
	if (!$_W['isajax']) {
		$_W['isw7_sign'] = true;
	}
	$accesstoken = cloud_oauth_token(safe_gpc_string($_GPC['code']));
	if (empty($accesstoken) || is_error($accesstoken)) {
		$message = '授权用户登录获取accesstoken失败，请联系管理员处理。详情：' . $accesstoken['message'];
		$_W['isajax'] ? iajax(-1, $message) : message($message, 'https://console.w7.cc', 'error');
	}
	$cloud_user_info = cloud_oauth_user($accesstoken['access_token']);
	if (is_error($cloud_user_info)) {
		$_W['isajax'] ? iajax(-1, $cloud_user_info['message']) : message($cloud_user_info['message'], 'https://console.w7.cc', 'error');
	}
	$bind_info = pdo_get('users_bind', array('bind_sign' => $cloud_user_info['openid'], 'third_type' => USER_REGISTER_TYPE_CONSOLE));
	$user_if_exists = pdo_exists('users', array('uid' => $cloud_user_info['out_uid']));
	if (!empty($cloud_user_info['out_uid']) && !empty($user_if_exists) || -1 == $cloud_user_info['out_uid']) {
		$user_id = -1 == $cloud_user_info['out_uid'] ? current(explode(',', $_W['config']['setting']['founder'])) : $cloud_user_info['out_uid'];
		if (empty($bind_info) && -1 != $cloud_user_info['out_uid']) {
			//删除之前的旧数据并插入新数据
			pdo_delete('users_bind', array('uid' => $user_id, 'third_type' => USER_REGISTER_TYPE_CONSOLE));
			pdo_insert('users_bind', array('uid' => $user_id, 'bind_sign' => $cloud_user_info['openid'], 'third_nickname' => $cloud_user_info['username'], 'third_type' => USER_REGISTER_TYPE_CONSOLE));
		}
	} elseif (!empty($bind_info) && !empty($bind_info['uid'])) {
		$user_id = $bind_info['uid'];
	} else {
		$str = random(8);
		$user_info = array(
			'username' => user_check(array('username' => $cloud_user_info['username'])) ? ($cloud_user_info['username'] . random(4)) : $cloud_user_info['username'],
			'password' => $str,
			'repassword' => $str,
			'remark' => '',
			'starttime' => TIMESTAMP,
			'founder_groupid' => 0,
			'groupid' => empty($_W['setting']['register']['groupid']) ? 0 : safe_gpc_int($_W['setting']['register']['groupid']),
		);
		if (ACCOUNT_MANAGE_NAME_VICE_FOUNDER_RULE == $cloud_user_info['role_identify']) {
			$user_info['founder_groupid'] = 2;
			$user_info['groupid'] = 0;
		}
		$user_save_result = user_info_save($user_info);
		if (is_error($user_save_result)) {
			message($user_save_result['message'], '', 'info');
		}
		pdo_insert('users_bind', array('uid' => $user_save_result['uid'], 'bind_sign' => $cloud_user_info['openid'], 'third_nickname' => $cloud_user_info['username'], 'third_type' => USER_REGISTER_TYPE_CONSOLE));
		$user_id = $user_save_result['uid'];
	}
	
	$user_info = user_single($user_id);
	if (empty($user_info)) {
		$_W['isajax'] ? iajax(-1, '用户不存在，请联系管理员处理！') : message('用户不存在，请联系管理员处理！', 'https://console.w7.cc', 'error');
	}
	if (USER_STATUS_CHECK == $user_info['status'] || USER_STATUS_BAN == $user_info['status']) {
		$_W['isajax'] ? iajax(-1, '您的账号正在审核或是已经被系统禁止，请联系网站管理员解决') : message('您的账号正在审核或是已经被系统禁止，请联系网站管理员解决', 'https://console.w7.cc', 'error');
	}
	if ($_W['role'] == ACCOUNT_MANAGE_NAME_EXPIRED || $_W['highest_role'] == ACCOUNT_MANAGE_NAME_EXPIRED) {
		$_W['isajax'] ? iajax(-1, '您的账号已过期，请联系管理员处理！') : message('您的账号已过期，请联系管理员处理！', 'https://console.w7.cc', 'error');
	}
	if (ACCOUNT_MANAGE_NAME_GENERAL_RULE == $cloud_user_info['role_identify'] && ACCOUNT_MANAGE_GROUP_VICE_FOUNDER == $user_info['founder_groupid']) {
		$result = cloud_bind_user_token(array('access_token' => $accesstoken, 'out_user_id' => $user_id, 'role_identify' => 'role2'));
		if (is_error($result)) {
			message($result['message'], 'https://console.w7.cc', 'error');
		}
		message('您为副创始人，角色转变中，稍后请重新进入该站点', 'https://console.w7.cc', 'error');
	}
	$update_data = array('lastvisit' => TIMESTAMP, 'lastip' => $_W['clientip']);
	switch ($cloud_user_info['role_identify']) {
		case ACCOUNT_MANAGE_NAME_VICE_FOUNDER_RULE:
			$update_data['founder_groupid'] = 2;
			break;
		case ACCOUNT_MANAGE_NAME_GENERAL_RULE:
			$update_data['founder_groupid'] = 0;
			break;
	}
	pdo_update('users', $update_data, array('uid' => $user_info['uid']));
	$w7_user_token = authcode(json_encode(array(
		'uid' => $user_info['uid'],
		'hash' => !empty($user_info['hash']) ? $user_info['hash'] : md5($user_info['password'] . $user_info['salt'])
	)), 'encode');
	if ($_W['isajax']) {
		iajax(0, $w7_user_token);
	}
	isetcookie('__w7sign', $w7_user_token);
	isetcookie('__session', $w7_user_token);
	$url = !empty($_GPC['referer']) ? safe_gpc_url($_GPC['referer']) : ($_W['siteroot'] . 'web/home.php?');
	header('Location:' . $url);
	exit;
}
if (!empty($_W['uid']) && 'bind' != $_GPC['handle_type'] && empty($_W['isw7_sign'])) {
	if ($_W['isajax']) {
		iajax(-1, '请先退出再登录！');
	}
	itoast('', $_W['siteroot'] . 'web/home.php');
}
$support_login_types = OAuth2Client::supportThirdLoginType();
if (checksubmit() || $_W['isajax'] || in_array($_GPC['login_type'], $support_login_types)) {
	if ($_W['isajax'] && STATUS_OFF == $_W['ishttps'] && empty($_W['setting']['copyright']['local_install'])) {
		iajax(-1, '该站点没有配置https，该功能无法正常使用，请联系站点管理员处理。');
	}
	_login($_GPC['referer']);
}
if (!empty($_W['setting']['copyright']['old_user_guide_status'])) {
	$cloud_invite_url = cloud_console_invite_url('/web/' . url('user/third-bind/console'));
	$_W['isw7_sign'] = true;
	$extend_buttons = array(
		'cancel' => array('url' => $_W['siteroot'], 'class' => 'btn btn-default', 'title' => '取消', ),
		'gosee' => array('url' => $cloud_invite_url['share_url'], 'class' => 'btn btn-primary', 'title' => '去看看'),
	);
	$content = '<div style="width: 63%;text-align: left;margin: 0 auto;">
	<p>1、原系统登录页已弃用，您需要注册/登录控制台后绑定该系统账号进行管理。</p>
	<p>2、已绑定过的可直接点击 <a href="https://c.w7.com" class="color-default" target="_blank">c.w7.com</a> 进行登录。</p>
	</div>';
	message($content, 'https://console.w7.cc', 'expired', false, $extend_buttons);
}
if (empty($_W['setting']['copyright']['local_install'])) {
	header('Location: https://console.w7.cc');
	exit;
}
$setting = $_W['setting'];
$_GPC['login_type'] = !empty($_GPC['login_type']) ? $_GPC['login_type'] : 'system';

$login_urls = user_support_urls();
$login_template = !empty($_W['setting']['basic']['login_template']) ? $_W['setting']['basic']['login_template'] : 'base';
template('user/login-' . $login_template);

function _login($forward = '') {
	global $_GPC, $_W;
	if (empty($_GPC['login_type'])) {
		$_GPC['login_type'] = 'system';
	}

	if (empty($_GPC['handle_type'])) {
		$_GPC['handle_type'] = 'login';
	}

	if ('login' == $_GPC['handle_type']) {
		$member = OAuth2Client::create($_GPC['login_type'], $_W['setting']['thirdlogin'][$_GPC['login_type']]['appid'], $_W['setting']['thirdlogin'][$_GPC['login_type']]['appsecret'])->login();
	} else {
		$member = OAuth2Client::create($_GPC['login_type'], $_W['setting']['thirdlogin'][$_GPC['login_type']]['appid'], $_W['setting']['thirdlogin'][$_GPC['login_type']]['appsecret'])->bind();
	}

	if (!empty($_W['user']) && '' != $_GPC['handle_type'] && 'bind' == $_GPC['handle_type']) {
		if (is_error($member)) {
			if ($_W['isajax']) {
				iajax(-1, $member['message'], url('user/profile/bind'));
			}
			itoast($member['message'], url('user/profile/bind'), '');
		} else {
			if ($_W['isajax']) {
				iajax(1, '绑定成功', url('user/profile/bind'));
			}
			itoast('绑定成功', url('user/profile/bind'), '');
		}
	}

	if (is_error($member)) {
		if ($_W['isajax']) {
			iajax(-1, $member['message'], url('user/login'));
		}
		itoast($member['message'], url('user/login'), '');
	}

	$record = user_single($member);
	$failed = pdo_get('users_failed_login', array('username' => safe_gpc_string($_GPC['username'])));
	if (!empty($record)) {
		if (USER_STATUS_CHECK == $record['status'] || USER_STATUS_BAN == $record['status']) {
			if ($_W['isajax']) {
				iajax(-1, '您的账号正在审核或是已经被系统禁止，请联系网站管理员解决', url('user/login'));
			}
			itoast('您的账号正在审核或是已经被系统禁止，请联系网站管理员解决', url('user/login'), '');
		}
		$_W['uid'] = $record['uid'];
		$_W['isfounder'] = user_is_founder($record['uid']);
		$_W['isadmin'] = user_is_founder($_W['uid'], true);
		$_W['user'] = $record;

		if (!empty($_W['isadmin']) && empty($_W['setting']['copyright']['local_install'])) {
			$redirect = url('user/third-bind/console_url', array('type' => 'we7'));
			if (empty($_W['setting']['founder_access_console'])) {
				setting_save(1, 'founder_access_console');
				$extend_buttons = array();
				$extend_buttons['status_console_button'] = array(
					'url' => $redirect,
					'class' => 'btn btn-default',
					'title' => '我已知晓，立即跳转',
				);
				$message = '创始人功能操作已迁移至微擎控制台，请登录微擎控制台操作<br/>如需进入平台内管理，请在首页内点击进入客户端';
				if ($_W['isajax']) {
					$data = array(
						'status' => -1,
						'message' => $message,
						'extend_buttons' => $extend_buttons,
						'redirect' => $redirect,
					);
					iajax(-4, $data);
				}
				message($message, $redirect, 'expired', '', $extend_buttons);
			} else {
				if ($_W['isajax']) {
					iajax(-4, '', $redirect);
				}
				itoast('', $redirect);
			}
		}

		if (empty($record['console']) && empty($_W['isadmin'])) {
			$cloud_bind_info = cloud_bind_user_info($record['uid']);
			if (is_error($cloud_bind_info)) {
				iajax(-1, $cloud_bind_info['message']);
			}
			if (!empty($cloud_bind_info['bind_status']) && !empty($cloud_bind_info['bind_mobile'])) {
				$user_bind = pdo_get('users_bind', array('uid' => $record['uid'], 'third_type' => USER_REGISTER_TYPE_CONSOLE));
				if (empty($user_bind)) {
					pdo_insert('users_bind', array('uid' => $record['uid'], 'bind_sign' => $cloud_bind_info['bind_mobile'], 'third_type' => USER_REGISTER_TYPE_CONSOLE, 'third_nickname' => $cloud_bind_info['bind_mobile']));
				} else {
					pdo_update('users_bind', array('bind_sign' => $cloud_bind_info['bind_mobile'], 'third_nickname' => $cloud_bind_info['bind_mobile']), array('id' => $user_bind['id']));
				}
			}
		}

		$support_login_bind_types = Oauth2CLient::supportThirdLoginBindType();
		if (in_array($_GPC['login_type'], $support_login_bind_types) && !empty($_W['setting']['copyright']['oauth_bind']) && !$record['is_bind'] && empty($_W['isfounder']) && (USER_REGISTER_TYPE_QQ == $record['register_type'] || USER_REGISTER_TYPE_WECHAT == $record['register_type'])) {
			if ($_W['isajax']) {
				iajax(-1, '您还没有注册账号，请前往注册');
			}
			message('您还没有注册账号，请前往注册', url('user/third-bind/bind_oauth', array('uid' => $record['uid'], 'openid' => $record['openid'], 'register_type' => $record['register_type'])));
			exit;
		}

		if (!empty($_W['siteclose']) && empty($_W['isfounder'])) {
			if ($_W['isajax']) {
				iajax(-1, '站点已关闭，关闭原因:' . $_W['setting']['copyright']['reason']);
			}
			itoast('站点已关闭，关闭原因:' . $_W['setting']['copyright']['reason'], '', '');
		}
		if (isset($_W['setting']['copyright']['log_status']) && $_W['setting']['copyright']['log_status'] == STATUS_ON) {
			$login_log = array(
				'uid' => $_W['uid'],
				'ip' => $_W['clientip'],
				'city' => isset($local['data']['city']) ? $local['data']['city'] : '',
				'createtime' => TIMESTAMP
			);
			table('users_login_logs')->fill($login_log)->save();
		}

		if ((empty($_W['isfounder']) || user_is_vice_founder()) && $_W['user']['is_expired']) {
			$user_expire = setting_load('user_expire');
			$user_expire = !empty($user_expire['user_expire']) ? $user_expire['user_expire'] : array();
			$notice = !empty($user_expire['notice']) ? $user_expire['notice'] : '您的账号已到期，请联系管理员!';
			$redirect = '';
			$extend_buttons = array();
			$extend_buttons['cancel'] = array(
				'url' => '',
				'class' => 'btn btn-default',
				'title' => '取消',
			);
			if ($_W['isajax']) {
				$message = array(
					'status' => -1,
					'message' => $notice,
					'extend_buttons' => $extend_buttons,
					'redirect' => $redirect,
				);
				iajax(0, $message);
			}
			message($notice, $redirect, 'expired', '', $extend_buttons);
		}

		$cookie = array();
		$cookie['uid'] = $record['uid'];
		$cookie['lastvisit'] = $record['lastvisit'];
		$cookie['lastip'] = $record['lastip'];
		$cookie['hash'] = !empty($record['hash']) ? $record['hash'] : md5($record['password'] . $record['salt']);
		$cookie['rember'] = safe_gpc_int($_GPC['rember']);
		$session = authcode(json_encode($cookie), 'encode');
		isetcookie('__session', $session, !empty($_GPC['rember']) ? 7 * 86400 : 0, true);
		pdo_update('users', array('lastvisit' => TIMESTAMP, 'lastip' => $_W['clientip']), array('uid' => $record['uid']));

		if (empty($forward)) {
			$forward = user_login_forward($_GPC['forward']);
		}
		// 只能跳到本域名下
		$forward = safe_gpc_url($forward);

		if ($record['uid'] != $_GPC['__uid']) {
			isetcookie('__uniacid', '', -7 * 86400);
			isetcookie('__uid', '', -7 * 86400);
		}
		if (!empty($failed)) {
			pdo_delete('users_failed_login', array('id' => $failed['id']));
		}
		cache_build_frame_menu();
		if ($_W['isajax']) {
			iajax(0, "欢迎回来，{$record['username']}", $forward);
		}
		itoast("欢迎回来，{$record['username']}", $forward, 'success');
	} else {
		if (empty($failed)) {
			pdo_insert('users_failed_login', array('ip' => $_W['clientip'], 'username' => safe_gpc_string($_GPC['username']), 'count' => '1', 'lastupdate' => TIMESTAMP));
		} else {
			pdo_update('users_failed_login', array('count' => $failed['count'] + 1, 'lastupdate' => TIMESTAMP), array('id' => $failed['id']));
		}
		if ($_W['isajax']) {
			iajax(-1, '登录失败，请检查您输入的账号和密码');
		}
		itoast('登录失败，请检查您输入的账号和密码', '', '');
	}
}
