<?php

namespace We7\V219;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1582846675
 * @version 2.1.9
 */

class UpdateMaterialStructure {

	/**
	 *  执行更新
	 */
	public function up() {
		pdo_query("ALTER TABLE " . tablename('wechat_news') . " MODIFY COLUMN  `content` MEDIUMTEXT;");
	}

	/**
	 *  回滚更新
	 */
	public function down() {


	}
}
